package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
		int lowerbound = number.getLowerbound();
		int upperbound = number.getUpperbound();

        int l = lowerbound;
        int u = upperbound;

        return recFindNumber(number, l, u);
    }

    private int recFindNumber(RandomNumber n, int left, int right){
        int mid = (right+left) / 2;
        int guess = n.guess(mid);

        if(guess == -1){
            return recFindNumber(n, mid + 1, right);
        }else if(guess == 1){
            return recFindNumber(n, left, mid - 1);
        }

        return mid;
    }
}
