package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        int n = list.size();

        if (n == 0) return 0;

        return list.remove(n - 1) + sum(list);
    }
}
