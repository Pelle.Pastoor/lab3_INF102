package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        int n = numbers.size() - 1;

        if (n==0) return numbers.get(n);

        if (numbers.get(0) > numbers.get(1)) return numbers.get(0);

        if (numbers.get(n) > numbers.get(n-1)) return numbers.get(n);

        return findPeak(numbers, 0, n);
    }

    private int findPeak(List<Integer> numbers, int left, int right){
        int mid = (left + right) / 2;

        int prev = numbers.get(mid-1);
        int current = numbers.get(mid);
        int next = numbers.get(mid+1);

        if(isPeak(prev, current, next)) return current;

        if (current > next) {
            return findPeak(numbers, left, mid);
        } else {
            return findPeak(numbers, mid + 1, right);
        }
    }

    private boolean isPeak(int prev, int current, int next){
        return current >= prev && current >= next;
    }
}
